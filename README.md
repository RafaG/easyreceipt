**EasyReceipt** es un software de escritorio desarrollado en C++ con el [framework Qt](https://es.wikipedia.org/wiki/Qt_(biblioteca)). El programa ayuda a realizar el **reparto del coste de recibos** (compra, luz, agua) de quienes comparten gastos.

EasyReceipt permite gestionar, guardar y cargar de manera ágil y sencilla varios recibos para calcular cuánto tendría que abonar cada persona.

![screenshot1.png](https://bitbucket.org/repo/9eRqn7/images/1986947248-screenshot1.png)
![screenshot2.png](https://bitbucket.org/repo/9eRqn7/images/2162755614-screenshot2.png)